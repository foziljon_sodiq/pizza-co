import { useState } from 'react';
import {
  Form,
  redirect,
  useActionData,
  useNavigate,
  useNavigation,
} from 'react-router-dom';
import { createOrder } from '../../services/apiRestaurant';
import Button from '../../ui/Button';
import { useDispatch, useSelector } from 'react-redux';
import {clearCart, getCart} from "../cart/cartSlice"
import EmptyCart from "../cart/EmptyCart"
import store from "../../store"
import { fetchAddress } from '../user/userSlice';

// https://uibakery.io/regex-library/phone-number
const isValidPhone = (str) =>
  /^\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$/.test(
    str,
  );

const fakeCart = [
  {
    pizzaId: 12,
    name: 'Mediterranean',
    quantity: 2,
    unitPrice: 16,
    totalPrice: 32,
  },
  {
    pizzaId: 6,
    name: 'Vegetale',
    quantity: 1,
    unitPrice: 13,
    totalPrice: 13,
  },
  {
    pizzaId: 11,
    name: 'Spinach and Mushroom',
    quantity: 1,
    unitPrice: 15,
    totalPrice: 15,
  },
];

function CreateOrder() {
  const dispatch = useDispatch()
  // custom
  const navigation = useNavigation();
  const isSubmitting = navigation.state == 'submitting';
  // const cart = fakeCart;
  const username = useSelector((state) => state.user.username);

  const formErrors = useActionData();
  
  // const [withPriority, setWithPriority] = useState(false);

  const cart = useSelector(getCart)
  if(!cart.length) return <EmptyCart/>

  return (
    <div className="px-4 py-6">
      <h2 className="mb-8 text-xl font-semibold">Ready to order? Let's  go!</h2>

      <Form method="POST">
        <div className="mb-5 flex grow flex-col  gap-2 gap-2 sm:flex-row sm:items-center">
          <label className="sm:basis-40">First Name</label>
          <input
            defaultValue={username}
            type="text"
            name="customer"
            required
            className="input  grow"
          />
        </div>

        <div className="mb-5 flex flex-col gap-2  gap-2 sm:flex-row sm:items-center">
          <label className="sm:basis-40">Phone number</label>
          <div className="grow">
            <input type="tel" name="phone" required className="input w-full" />
          </div>
          {/* {formErrors.phone && <p>{formErrors.phone}</p>} */}
        </div>

        <div className="mb-5 flex flex-col gap-2  gap-2 sm:flex-row sm:items-center">
          <label className="sm:basis-40">Address</label>
          <div className="grow">
            <input
              type="text"
              name="address"
              required
              className="input w-full"
            />
          </div>
          <button onClick={() => dispatch(fetchAddress())}>Get Postion</button>
        </div>

        <div className="mb-12 flex items-center">
          <input
            className="h-6 w-6 cursor-pointer accent-yellow-400"
            type="checkbox"
            name="priority"
            id="priority"
            // value={withPriority}
            // onChange={(e) => setWithPriority(e.target.checked)}
          />
          <label className="ml-3 font-medium" htmlFor="priority">
            Want to yo give your order priority?
          </label>
        </div>

        <div>
          <input type="hidden" name="cart" value={JSON.stringify(cart)} />
          <Button type="primary" disabled={isSubmitting}>
            {isSubmitting ? 'Placing order...' : 'Order now'}
          </Button>
        </div>
      </Form>
    </div>
  );
}
export async function action({ request }) {
  const formData = await request.formData();
  const data = Object.fromEntries(formData);
  console.log(data);

  const order = {
    ...data,
    cart: JSON.parse(data.cart),
    priority: data.priority == 'on',
  };

  const errors = {};

  if (!isValidPhone(order.phone)) {
    errors.phone =
      'Please give us your correct phone number, We might need it to contact you.';
  }

  if (Object.keys(errors).length > 0) return errors;
  // if everythings okey create order
  
  const newOrder = await createOrder(order);

  store.dispatch(clearCart()); 

  return redirect(`/order/${newOrder.id}`);
}
export default CreateOrder;
