import React, { useState } from 'react';
import { useNavigate } from 'react-router';

const SearchOrder = () => {
  const [query, setQuery] = useState('');
  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();

    if (!query) return;

    navigate('/order/' + query);
    setQuery('');
  }

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        placeholder="Search order "
        className="placeholder:text-color-400 sm-w-64 rounded-full 
          bg-yellow-100 px-4 py-2 text-sm transition-all duration-300 
          focus:w-72 focus:outline-none active:outline-none sm:w-64"
      />
    </form>
  );
};

export default SearchOrder;
