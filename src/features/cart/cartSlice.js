const { createSlice } = require('@reduxjs/toolkit');

const initialState = {
  // cart: [],
  cart: [ ],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem(state, action) {
      state.cart.push(action.payload);
    },
    deleteItem(state, action) {
      state.cart = state.cart.filter((item) => item.pizzaId != action.payload);
    },
    increateItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId == action.payload);
      item.quantity++;
      item.totalPrice = item.quantity * item.unitPrice;
    },
    decreaseItemQuantity(state, action) {
      const item = state.cart.find((item) => item.pizzaId == action.payload);
      item.quantity--;
      item.totalPrice = item.quantity * item.unitPrice;

      if(item.quantity == 0) cartSlice.caseReducers.deleteItem(state,action)
    },
    clearCart(state, action) {
      state.cart = [];
    },
  },
});

export const {
  addItem,
  deleteItem,
  clearCart,
  increateItemQuantity,
  decreaseItemQuantity,
} = cartSlice.actions;

export default cartSlice.reducer;

export const getTotalCartQuantity = () => {
  return (state) => state.cart.cart.reduce((sum, item) => sum + item.quantity, 0);
};

export const getTotalCartPrice = () => {
  return (state) => state.cart.cart.reduce((sum, item) => sum + item.totalPrice, 0);
};

export const getCart = state => state.cart.cart;

export const getCurrentQuantityById = (id) => {
    return (state) => state.cart.cart.find((item)=> item.pizzaId == id)?.quantity ?? 0 }
  