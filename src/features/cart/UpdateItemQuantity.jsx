import React from 'react'
import Button from '../../ui/Button'
import { decreaseItemQuantity, increateItemQuantity } from './cartSlice'
import { useDispatch } from 'react-redux'

const UpdateItemQuantity = ({pizzaId, currentQuantity}) => {
    const dispatch = useDispatch()
  return (
    <div className='flex items-center gap-2 md:gap-3'>
      <Button type="round" onClick={()=> dispatch(increateItemQuantity(pizzaId))}>+</Button>
      <span className='font-sm font-medium'>{currentQuantity}</span>
      <Button type="round" onClick={()=> dispatch(decreaseItemQuantity(pizzaId))}>-</Button>
    </div>
  )
}

export default UpdateItemQuantity
