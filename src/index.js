import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App'; 
import { Provider } from 'react-redux';
import store from './store';
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
);
 

// what is tailwind css
// setting up tailwind
// working with color 
// styling text
// border box model 
// responsive desing
// using flexbox 
// using grid
//   styling button element, creating button element
// styling form element
// reusing styles, with @apply
// reusing styles with components, 
// position, configuration custom font
// 