import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Home from "./ui/Home";
import Error from "./ui/Error";
import Menu, { loader as menuLoader } from "./features/menu/Menu"
import Cart from "./features/cart/Cart"
import CreateOrder, {action as createOrderAction} from "./features/order/CreateOrder"
import Order, { loader as orderLoader } from "./features/order/Order"
import AppLayout from "./ui/AppLayout";


const router = createBrowserRouter([

  {
    element: <AppLayout />,
    errorElement: <Error />,
    children: [
      {
        path: "/",
        element: <Home />
      },
      {
        path: "/menu",
        element: <Menu />, 
        loader: menuLoader,
        errorElement: <Error />,
      }, 
      {
        path: "/cart",
        element: <Cart />,
        loader: menuLoader,
      },  
      {
        path: "/order/new",
        element: <CreateOrder />,
        action: createOrderAction,    
      },
      {
        path: "/order/:orderId",
        element: <Order />,
        loader: orderLoader,
        errorElement: <Error />,
      },
    ]
  }

])

function App() {
  return (
    <RouterProvider router={router} />
    // <div className="container mx-auto ">
    //   <h1 className="text-10xl font-serif px-2 py-3 sm:bg-red-500  bg-green-500">
    //     Hello world{' '}
    //   </h1>
    //   <p class="...  text-stone-100 font-light tracking-[5px]">The quick brown fox ...</p>
    //   <p class="...  text-stone-300 font-normal  ">The quick brown fox ...</p>
    //   <p class="...  text-stone-500 font-medium">The quick brown fox ...</p>
    //   <p class="... text-stone-700  font-semibold">The quick brown fox ...</p>
    //   <p class="...  text-stone-900 font-bold">The quick brown fox ...</p>
    //   <p class="...  font-bold">The quick brown fox ...</p>
    // </div>
  );
}

export default App;
