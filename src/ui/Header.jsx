import React from 'react';
import { Link } from 'react-router-dom';
import SearchOrder from '../features/order/SearchOrder';
import UserName from '../features/user/UserName';

const Header = () => {
 
  return ( 
    <header className='font-pizza flex items-center justify-around bg-yellow-400 uppercase px-4 py-3 border-b border-stone-500  sm:px-6'>
      <Link to="/" className='tracking-widest'> Fast Pizza Co.</Link>
 
      <SearchOrder />
      <UserName />
    </header>
  );
};

export default Header;
