import React, { Children } from 'react';
import {Link} from 'react-router-dom';
 
const Button = ({ children, disabled, to, type,onClick }) => {
   
  const base =
    'inline-block rounded-full   font-semibold uppercase tracking-wide text-stone-800 transition-colors duration-300  focus:ring focus:ring-yellow-300';
 
  const styles = {
    small: base + ' px-4 py-2 text-xs bg-yellow-400 focus:bg-yellow-300 hover:bg-yellow-300',
    primary: base + ' px-4 py-3 bg-yellow-400 focus:bg-yellow-300 hover:bg-yellow-300',
    secondary:
      base +' bg-transparent border-2 border-stone-300 px-4 py-3 hover:bg-stone-300 focus:bg-stone-300 focus:ring-stone-200 ',
    round: base + " px-3 py-1   bg-yellow-400 focus:bg-yellow-300 hover:bg-yellow-300",
  };
   
  if (to) {

    return   (
      <Link className={styles[type]} to={to}>
      {children}
      </Link>
    )
  }

  if (onClick) {

    return   (
      <button disabled={disabled} className={styles[type]} onClick={onClick}>
      {children}
    </button>
    )
  }

  return (
    <button disabled={disabled} className={styles[type]}>
      {children}
    </button>
  );
};

export default Button;
