/** @type {import('tailwindcss').Config} */
module.exports = { 
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: { 
    fontFamily:{
      robo:"Roboto Mono, monospace, sans-serif"
    },
     

    // fontFamily: {
    //   pizza: "Roboto Mono, monospace"
    // },
    extend: {
      colors: {
        pizza: "#123"
      },
      backgroundColor:{
        pizza: "#123" 
      },
    },
  },
  plugins: [],
};
