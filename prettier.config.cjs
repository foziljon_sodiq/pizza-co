module.exports = {
  plugins: ['prettier-plugin-tailwindcss'],
  tailwindAttributes: ['myClassList'],
  singleQuote: true,
};   